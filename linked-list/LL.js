/*
Task1: Given a Linked List of integers, write a function to modify the linked list such that all even numbers appear before all 
the odd numbers in the modified linked list. Also, keep the order of even and odd numbers same.

Task 2:Write a Count() function that counts the number of times a given int occurs in a list.
The code for this has the classic list traversal structure as demonstrated in Length().

Task 3:Write a Pop() function that is the inverse of Push(). Pop() takes a non-empty list, deletes the head node, 
and returns the head node's data. If all you ever used were Push() and Pop(), then our linked list would really look like a stack. 
However, we provide more general functions like GetNth() which what make our linked list more than just a stack.
Pop() should assert() fail if there is not a node to pop

Task 4:Write a SortedInsert() function which given a list that is sorted in increasing order,
and a single node, inserts the node into the correct sorted position in the list. While Push()
allocates a new node to add to the list, SortedInsert() takes an existing node, and just rearranges
pointers to insert it into the list.

Task 5: Write a RemoveDuplicates() function which takes a list sorted in increasing order and
deletes any duplicate nodes from the list. Ideally, the list should only be traversed once.

Author - Karan Kanwal
filename -LL.js 
*/
class Node{
    constructor(element){
        this.element = element;
        this.next = null;
    }
}

class LinkedList{

    

    constructor(){
        this.head= null;
        this.size =0;
    }

    //add element to the linkedlist
    add(element){
        var node = new Node(element);
    
        if(this.head==null){
            this.head = node;
        }else{
            let curr = this.head;
            while(curr.next!=null){
                curr = curr.next;
            }
            curr.next = node;
    
        }
    }

    //print the linkedlist
    print(){
        var curr = this.head;
        var res="";
        while(curr){
            res = res + " "+curr.element;
            curr = curr.next;
        }
        console.log(res);
    }

    //1-task
    task1(){
        var curr = this.head;
        var even = null;
        var prev;
        if(curr.element%2==0){
            even = curr;
            prev = curr;
            curr = curr.next;
        }

        while(curr !=null){
            if(curr.element%2 ==0){
                if(even == null){
                    prev.next = curr.next;
                    curr.next = this.head;
                    this.head = curr;
                    curr = prev.next;
                    even = this.head;
                }else{
                    if(prev == even){
                        prev = prev.next;
                        even = even.next;
                        curr = curr.next
                    }else{
                        prev.next = curr.next;
                        curr.next = even.next;
                        even.next = curr;
                        curr = prev.next;
                        even = even.next;
                    }
                }
            }else{
                prev = curr;
                curr = curr.next;
            }
        }
    }

    //count a particular element
    task2(n){
        
        var curr = this.head;
        var k=0;
        while(curr!=null){
            if(curr.element == n){
                k++;
            }
            curr = curr.next;
        }
        console.log(k);
    }

    //pop from the front
    task3(){
        var curr = this.head;
        if(curr==null){
            console.log("No elements present");
        }else{
            this.head = curr.next;
            console.log(curr.element);
        }
    }

    //sortedinsert
    task4(n){
        var node = new Node(n);
        var curr = this.head;
        var prev = null;
        
        while(curr!=null && curr.element < n){
            prev = curr;
            curr = curr.next;
        }   
        if(prev == null){
            node.next = curr;
            this.head = node;
        }else{
            node.next = curr;
            prev.next = node;
        }
    }
    
    task5(){
        var curr = this.head.next;
        var prev = this.head;
        var el;

        while(curr!=null){
            el = prev.element;
            if(el == curr.element){
                prev.next = curr.next;
                curr = prev.next;
            }else{
                prev = prev.next;
                curr = curr.next;
            }
        }
    }
}

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

prompts.question("Enter the numbers: ",function(n){
    var arr = n.split(' ');
    let l = new LinkedList();
    for(var i=0; i<arr.length; i++){
        l.add(parseInt(arr[i]));
    }
    l.task1();
    l.print();
    process.exit();
});

