/*
Task 1:Given a binary tree, Your task is to complete the function largestBst that returns the size of the
 largest subtree which is also a Binary Search Tree (BST). If the complete Binary Tree is BST, then return the size of whole tree.

Task 2:  Given a Binary Search Tree (BST) and a node no 'x' , your task is to delete the node 'x' from the BST .
You are required to complete the function deleteNode. You should not read any input from stdin/console. 
There are multiple test cases. For each test case, this method will be called individually.

Task 3: Given two BST, Your task is to complete the function merge which prints the elements of both BSTs in sorted form.

Task 4: Given a binary tree, return true if it is BST, else false

Task 5:  Given a binary tree, return true if it is BST, else false. 
For example, the following tree is not BST, because 11 is in left subtree of 10.

Author - Karan Kanwal
fileName - btree.js
*/

class Node{
    constructor(data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}

class bTree{
    constructor(){
        this.root = null;
    }

    insert(data){
        var node = new Node(data);
        
        if(this.root == null){
            this.root = node;
        }else{
            this.insertNode(this.root, node);
        }
    }

    insertNode(node, newNode){
        if(newNode.data < node.data){
            if(node.left == null){
                node.left = newNode;
            }else{
                this.insertNode(node.left, newNode);
            }
        }else{
            if(node.right == null){
                node.right = newNode;
            }else{
                this.insertNode(node.right, newNode);
            }
        }
    }

    levelOrder(root){
        var q = [];
        var res= [];
        q.push(root);

        while(q.length!=0){
            var temp = q.shift();
            res.push(temp.data);

            if(temp.left!=null){
                q.push(temp.left);
            }
            if(temp.right!=null){
                q.push(temp.right);
            }
            
        }
        return res;
    }

    //task 1 is in tree file

    //task 2
    //min element for task 2
    minElem(root){
        min = root.data;
        while(root.left!=null){
            min = root.left.data;
            root = root.left;
        }
        return min;
    }

    deleteNode(root, data){


        if(root == null){
            return root;
        }

        if(data<root.data){
            root.left = this.deleteNode(root.left, data);
        }else if(data>root.data){
            root.right = this.deleteNode(root.right,data);
        }else{
            //no child or one chlid
            if(root.left == null){
                return root.right;
            }else if(root.right == null){
                return root.left;
            }
            root.data= this.minElem(root.right);
            root.right = this.deleteNode(root.right, root.data);
        }
        return root;
    }

    delete(data){
        this.root = this.deleteNode(this.root,data);
    }

    //task 3 merge two trees
    inOrder(root){
        if(root == null){
            return;
        }
        this.inOrder(root.left);
        console.log(root.data);
        this.inOrder(root.right);
        
    }
    merge(root1, root2){
        if(root2 == null){
            return;
        }
        
        root1.insert(root.left);
        root1.insert(root.right);
    }

    //task 4, check if the tree is BST or not
    checkBST(root){
        if(root==null){
           return true;
        }
        if(root.left!=null && root.left.data> root.data){
           return false;
        }
        if(root.right!=null && root.right.data < root.data){
           return false;
        }
        if(!this.checkBST(root.right) || !this.checkBST(root.left)){
           return false;
        }
        return true;
    }


}


var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

prompts.question("Enter the data: ",function(nos){
    var arr = nos.split(',');
    var tree = new bTree();
    for(var i=0; i<arr.length; i++){
        tree.insert( arr[i]);
    }
    
    process.exit();
});

//for task 3
// prompts.question("enter the data: ",function(nos){
//     prompts.question("enter the data for 2nd tree: ",function(ns){
//         var arr1 = nos.split(',');
//         var arr2 = ns.split(',');

//         var tree1 = new bTree();
//         var tree2 = new bTree();

//         for(var i=0; i<arr1.length; i++){
//             tree1.insert(arr1[i]);
//         }

//         for(var i=0; i<arr2.length; i++){
//             tree1.insert(arr2[i]);
//         }

//         tree1.inOrder(tree1.root);
//         tree1.merge(tree1.root, tree2.root);
//         tree1.inOrder(tree1.root);
//         process.exit();
//     });
// });















