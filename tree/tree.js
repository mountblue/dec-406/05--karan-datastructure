/* 
Task 1: Given a Binary Tree you need to find maximum value which you can get by subtracting value of node B 
from value of node A, where A and B are two nodes of the binary tree and A is an ancestor of B .
You are required to complete the function maxDiff . You should not read any input from stdin/console.
There are multiple test cases. For each test case, this method will be called individually.

Task 2: . Given a Complete Binary tree, print the level order traversal in sorted order.

Task 3: Given a binary tree, your task is to complete the function findRightSibling(), 
that should return the right sibling to a given node if it doesn’t exist return null.

Task 4: Given a binary tree of size N and two nodes. Your task is to complete the function NumberOFTurn() 
that should return the count of the number of turns needs to reach from one node to another node of the Binary tree.

Task 5:  Given a binary tree of size N+1, your task is to complete the function tiltTree(),
that return the tilt of the whole tree. The tilt of a tree node is defined as the absolute 
difference between the sum of all left subtree node values and the sum of all right subtree node values.
Null nodes are assigned tilt to be zero. Therefore, tilt of the whole tree is defined as the sum of all nodes’ tilt.

Author - Karan Kanwal
filename - tree.js
*/

class Node{
    constructor(data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}

class Tree{
    constructor(){
        this.root = null;
    }

    insert(data){
          var node = new Node(data);
          var queue = [];

          if(this.root ==  null){
            this.root = node;
          }else{
              queue.push(this.root);

              while(queue.length!=0){
                var temp = queue.shift();

                if(temp.left == null){
                    temp.left = node;
                    break;
                }else{
                    queue.push(temp.left);
                }

                if(temp.right == null){
                    temp.right = node;
                    break;
                }else{
                    queue.push(temp.right);
                }
              }
          }
    
        }

        //level-order printing
        levelOrder(root){
            var q = [];
            var res= [];
            q.push(root);

            while(q.length!=0){
                var temp = q.shift();
                res.push(temp.data);

                if(temp.left!=null){
                    q.push(temp.left);
                }
                if(temp.right!=null){
                    q.push(temp.right);
                }
                
            }
            return res;
        }

        //min in a tree
        min(root){
            var min = root.data;
            var l;
            var r;
            var arr=[];
            if(root.left!=null){
                l = min(root.left);
            }
            if(root.right!=null){
                r = min(root.right);
            }

            if(l.data< min){
                min = l.data;
            }
            if(r.data<min){
                min = r.data;
            }
            return min;

        }

        //task1 find maxdiff
       
        //task 2
        task2(){
            var q = [];
            var res= [];
            q.push(this.root);

            while(q.length!=0){
                var temp = q.shift();
                res.push(temp.data);

                if(temp.right!=null){
                    q.push(temp.right);
                }
                if(temp.left!=null){
                    q.push(temp.left);
                }
                
            }
            return res;
        }


        //task3
        findRightSibling(data){
            var temp = this.root;
            var q= [];
            q.push(temp, null);
            var prev = null;
            while(q.length!=0){
                var curr = q.shift();
                if(curr!=null && curr.data == data){
                    while(curr!=null){
                        prev = curr;
                        curr = q.shift();
                    }
                    if(prev == null || prev.data==data){
                        return null;
                    }else{
                        return prev.data;
                    }
                }else{
                    if(curr == null){
                        if(q.length == 0){
                            break;
                        }else{
                        q.push(null);
                        }
                    }else{
                        if(curr.left != null){
                            q.push(curr.left);
                        }
                        if(curr.right != null){
                            q.push(curr.right);
                        }
                    }
                }
            }
        }
                  
         //task 4
         LCA(root, m,n){
            var node1 = new Node(m);
            var node2 = new Node(n);

            if(root == null){
                return null;
            }
            if(root==node1 || root == node2){
                return root;
            }

            leftlca = this.LCA(root.left, m,n);
            rightlca = this.LCA(root.right,m,n);

            if(leftlca!=null && rightlca!=null){
                return root;
            }

            return (leftlca !=null)?leftlca:rightlca; 
         }

                                                                
        
         //task 5
        sumOfTree(root){
            if(root == null){
                return 0;
            }
            return root.data + this.sumOfTree(root.left) + this.sumOfTree(root.right);
        }

        tiltTree(root){
            if(root==null){
                return 0;
            }
            return Math.abs(this.sumOfTree(root.left)- this.sumOfTree(root.right)) + this.tiltTree(root.left) +this.tiltTree(root.right);
        }
        
         //BST task 1
         checkBST(root){
             if(root==null){
                return true;
             }
             if(root.left!=null && root.left.data> root.data){
                return false;
             }
             if(root.right!=null && root.right.data < root.data){
                return false;
             }
             if(!this.checkBST(root.right) || !this.checkBST(root.left)){
                return false;
             }
             return true;
         }
        
}



var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

prompts.question("enter the data: ",function(nos){
    var arr = nos.split(',');
    var tree = new Tree();

    for(var i=0; i<arr.length; i++){
        tree.insert(parseInt(arr[i]));
    }

    console.log(tree.checkBST(tree.root));
    process.exit();
});






















