/*
Given two arrays and a number x, find the pair whose sum is closest to x and the pair has an element from each array.

Author - Karan Kanwal
filename - 5.js
*/ 

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

prompts.question("Enter 1st array ",function(n1){
    prompts.question("Enter 2nd array ",function(n2){
        prompts.question("Enter the sum ",function(n){
            arr1 = n1.split(',');
            arr2 = n2.split(',');
            arr1 = arr1.sort();
            arr2 = arr2.sort();
            n = parseInt(n);
            var arr = [arr1[0],arr2[0]];
            var min = Math.abs(n-(parseInt(arr1[0])+parseInt(arr2[0])));

            for(var i=0; i<arr1.length; i++){
                for(var j=0; j<arr2.length; j++){
                    var diff = Math.abs(n-(parseInt(arr1[i])+parseInt(arr2[j])));
                    if(diff<min){
                        min = diff;
                        arr[0]= arr1[i];
                        arr[1]= arr2[j];
                    }
                }
            }
            console.log(arr);
            process.exit();
        });
    });
});