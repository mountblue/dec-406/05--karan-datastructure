/*
 Write a program to input a list of n integers in an array and arrange 
 them in a way similar to the to-and-fro movement of a Pendulum.

Author - Karan Kanwal
filename - 4.js
*/ 

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

prompts.question("enter an array ", function(n){
    var arr = n.split(',');
    arr = arr.sort();
    var arr1 = [];
    var len = arr.length;
    for(var i=0; i<len; i++){
        if(len%2 === 0){
            if(i%2===0){
                arr1.push(arr[i]);
            }else{
                arr1.unshift(arr[i]);
            }
        }else{
            if(i%2===0){
                arr1.unshift(arr[i]);
            }else{
                arr1.push(arr[i]);
            }
        }
    }
    console.log(arr1);
    process.exit();
});