/*
Given an increasing sequence a[], we need to find the K-th missing contiguous 
element in the increasing sequence which is not present in the sequence.
 If no k-th missing element is there output -1.

Author - Karan Kanwal
filename - 3.js
*/  

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

prompts.question("enter your sequence ", function(n){
    prompts.question("enter the k value ", function(k){
        var arr1 = n.split(',');
        var x=1;
        var total=0;
        for(var i=0; i<arr1.length; ){
            if(x===parseInt(arr1[i])){
                x++;
                i++;
            }else{
                x++;
                total++;
            }
            if(total===parseInt(k)){
                console.log(--x);
                process.exit();
            }
        }
        console.log("-1");
        process.exit();
    });
});