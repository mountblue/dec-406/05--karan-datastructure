/*
Problem - Given an array A of size N, construct a Sum Array S(of same size) such that S is equal 
to the sum of all the elements of A except A[i]. Your task is to complete the function SumArray(A, N)
which accepts the array A and N(size of array).

Author - Karan Kanwal
filename - 1.js
*/

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

arr=[];

prompts.question("Enter your nos ", function(n){
    var arr1 = n.split(",");
    var sum =0;
    for(var i=0; i<arr1.length; i++){
        sum+=parseInt(arr1[i]);
    }
    for(var i=0; i<arr1.length;i++){
        arr.push(sum-parseInt(arr1[i]));
    }

    console.log(arr);
    process.exit();
});








