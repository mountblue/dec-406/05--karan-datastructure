/*
This is a functional problem . Your task is to return the product of array elements under a given modulo.

The modulo operation finds the remainder after division of one number by another.
 For example, K(mod(m))=K%m= remainder obtained when K is divided by m.

Author - Karan Kanwal
filename - 2.js
*/

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

arr=[];

prompts.question("Enter your nos ", function(n){
    prompts.question("enter the modulo ",function(m){
        var multiplication=0;
        arr1 = n.split(",");
        for(var i=0; i<n; i++){
            multiplication*=parseInt(arr1[i]);
        }

        console.log(multiplication%m);
        process.exit();
    });
});