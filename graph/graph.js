/*
Task 1: Given a graph, check whether it is Biconnected or not.

Task 2: Mayank once came to New Delhi for an International Mathematics Conference. His mathematics skills were a great topic of
 discussion in those days. So a mathematician Harsh “Challenged” Mayank for a competition and Mayank accepted it.

Rules: They will ask 3 questions each and the one who will give all the answers right will be the winner.

Mayank answered all the questions very easily. Now its time for Harsh to answer the questions but Harsh was confused by the very
 first question now you have to help so let's read the question.

Question: Mayank asked that you are in a beautiful Castle which is huge and contains ’N’ number of rooms and each room contains 
'M' path to ‘M’ other rooms. There may be only one path which connects two room i.e either one path or no path. Mayank asked that 
you have to find whether you can reach back to the room from which you started after travelling all the paths but note that once you 
travel a path, then it will be closed and make sure all the rooms are travelled atleast once. In general all paths are to be travelled 
exactly once and each room must be travelled atleast once. Note: If any room is not connected print False and
 if there is only one room then print True.

Task 3: Given N * M string array of O's and X's

Return the number of 'X' total shapes. 'X' shape consists of one or more adjacent X's (diagonals not included).

Task 4: Given a connected acyclic graph with N nodes and N-1 edges, find out the pair of nodes that are at even distance from each other

Task 5: Given a directed graph your task is to complete the method isCycle to detect if there is a cycle in the graph or not. 
You should not read any input from stdin/console. 

Author - Karan Kanwal
fileName - btree.js
*/

class Graph{
    constructor(noOfVertices){
        this.noOfVertices = noOfVertices;
        this.AdjList = new Map();
    }

    addVertex(v){
        this.AdjList.set(v,[]);
    }

    addEdge(v,w){
        this.AdjList.get(v).push(w);
        this.AdjList.get(w).push(v);
    }

    printGraph(){
        //get all the vertices
        var getKeys =  this.AdjList.keys();

        for(var i of getKeys){
            var getValues = this.AdjList.get(i);
            var conc = " ";

        for(var j of getValues){
            conc+=j + " ";
        }

        console.log(i +"->"+ conc);
        }
    }
}


var g = new Graph();
var vertices = ['A','B','C','D','E','F'];

for(var i=0; i<vertices.length; i++){
    g.addVertex(vertices[i]);
}

g.addEdge('A','B');
g.addEdge('A','D');
g.addEdge('A','E');
g.addEdge('B','C');
g.addEdge('D','E');
g.addEdge('E','F');
g.addEdge('E','C');
g.addEdge('C','F');

g.printGraph();























