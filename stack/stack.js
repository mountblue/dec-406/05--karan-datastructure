/* 
Task 1: Delete the middle element of the stack.
Given a stack with push(), pop(), empty() operations, delete middle of it without using any additional data structure.

Task 2: Print Bracket Number. Given an expression exp of length n consisting of some brackets. 
The task is to print the bracket numbers when the expression is being parsed.

Task 3: Given array A[] of integers, the task is to complete the function findMaxDiff which finds 
the maximum absolute difference between nearest left and right smaller element of every element in array.
If the element is the leftmost element, nearest smaller element on left side is considered as 0. 
Similarly if the element is the rightmost elements, smaller element on right side is considered as 0.

Task 4: Given an array A of size N having distinct elements, the task is to find the next greater element for each element of
 the array in order of their appearance in the array. If no such element exists, output -1.

Task 5: Given an array of integers, find the nearest smaller number for every element such that the smaller 
element is on left side.If no small element present on the left print -1.

Author - Karan Kanwal
filename - tree.js
*/

class Stack{
    constructor(){
        this.items = [];
    }

    push(n){
        this.items.push(n);
    }

    pop(){
        if(this.items.length == 0){
            return "underflow";
        }else{
            return this.items.pop();
        }
    }

    peek(){
        if(this.items.length !=0 ){
            return this.items[this.items.length-1];
        }
    }

    length(){
        return this.items.length;
    }

    task1(s){
        var n = this.items.length;
        var x = Math.floor(n/2);

        var s1 = new Stack();
        for(var i=0; i<x; i++){
            s1.push(s.pop());
        }
        s.pop();
        for(var i=0; i<x; i++){
            s.push(s1.pop());
        }
    }

    task2(arr){
        var res = [];
        var val =0;
        var s = new Stack();
        for(var i=0; i<arr.length; i++){
            if(arr[i]=='('){
                val++;
                s.push(val);
                res.push(val);
            }else if(arr[i]==')'){
                res.push(s.pop());
            }
        }

        console.log(res);
    }
    
    //for task 3
    RS(arr){
        var s = new Stack();
        var res= new Array(arr.length);

        for(var i=arr.length-1;i>=0; i--){

            if(s.length !=0){
                while(s.length()!=0 && s.peek()>=arr[i]){
                    s.pop();
                }
            }

            res[i]= s.length() == 0? '-1':s.peek();
            s.push(arr[i]);

        }
        return res;
    }
    task3(arr){
        var s = new Stack();
        var LS = s.task5(arr);
        var RS = s.RS(arr);
        var maxdiff = -1;
        for(var i=0; i<arr.length; i++){
            var diff = Math.abs(LS[i] - RS[i]);
            if(diff > maxdiff){
                maxdiff = diff;
            }
        }
        return maxdiff;
    }

    task4(arr){
        var s = new Stack();
        var res= new Array(arr.length);

        for(var i=arr.length-1;i>=0; i--){

            if(s.length !=0){
                while(s.length()!=0 && s.peek()<=arr[i]){
                    s.pop();
                }
            }

            res[i]= s.length() == 0? '-1':s.peek();
            s.push(arr[i]);

        }
        return res;
    }
    
    task5(arr){
        var s = new Stack();
        var res = new Array(arr.length);

        for(var i=0; i<arr.length; i++){

            if(s.length!=0){
                while(s.length()!=0 && arr[i]<s.peek()){
                    s.pop();
                }
            }

            res[i] = s.length()==0 ? '-1': s.peek();
            s.push(arr[i]);

        }
        return res;


    }
}

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

prompts.question("enter the elements for the stack ", function(n){
    //remove space between ' ' for task 2
    var arr = n.split(' ');
    var s = new Stack();
    // var arr = [];
    // for(var i=0; i< temparr.length; i++){
    //     arr[i] = parseInt(temparr[i]);
    // }
    // comment the for loop for 2nd & 3rd task
    // for(var i=0; i<arr.length; i++){
    //     s.push(arr[i]);
    // }

    console.log(s.task3(arr));
    
    process.exit();
});