/*
Given a 2D matrix of size M*N. Traverse and print the matrix in spiral form.

Author - Karan Kanwal
filename - 1.js
*/

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

prompts.question("Enter the rows: ",function(m){
    prompts.question("Enter the columns: ", function(n){
        prompts.question("Enter the entries: ",function(nos){
            var tempArray = nos.split(' ');
            var arr=[];
            for (var i=0; i<m; i++){
                arr.push([]);
            }
            var x=0;
            for(var i=0; i<arr.length; i++){
                for(var j=x; j<x+parseInt(n); j++){
                    arr[i].push(tempArray[j]);
                }
                x+=parseInt(m);
            }
            console.log(arr);
            var r=0;
            var d=arr[0].length-1;
            var l=arr.length-1;
            var u=0;
            var dir='R';
            var res = [];

            while(r<=l && u<=d){
                if(dir=='R'){
                    for(var i=u; i<=d; i++){
                        res.push(arr[r][i]);                        
                    }
                    r++;
                    dir= 'D';
                }else if(dir=='D'){
                    for(var i=r; i<=l; i++){
                        res.push(arr[i][d]);
                    }
                    d--;
                    dir = 'L';
                }else if(dir=='L'){
                    for(var i=d; i>=u; i--){
                        res.push(arr[l][i]);
                    }
                    l--;
                    dir='U';
                }else if(dir=='U'){
                    for(var i=l; i>=r;i--){
                        res.push(arr[i][u]);
                    }
                    u++;
                    dir='R';
                }
                
            }
            
            console.log(res);
            process.exit();
        });
    });
});

