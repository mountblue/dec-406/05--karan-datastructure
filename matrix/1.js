/*
There is the Rectangular path for a Train to travel consisting of n and m rows and columns respectively. 
The train will start from one of grid cells and it will be given a command in the form of String s. consisting of characters
‘L’, ‘R’, ‘U’, ‘D’.The train will follow the instructions of the command string, where 'L' corresponds moving to the left,
'R' towards the right, 'U' for moving up, and 'D' means down.

Author - Karan Kanwal
filename - 1.js
*/

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

prompts.question("Enter no of rows: ", function(m){
    prompts.question("Enter no of columns: ", function(n){
        prompts.question("Enter the path to be followed ", function(a){
            var arr= a.split(',');
            var x=0;
            var y=0;
            n = parseInt(n);
            m = parseInt(m);

            for(var i=0; i<arr.length; i++){
                var c = arr[i];
                if(c=='L'){
                    x-=1;
                }else if(c=='R'){
                    x+=1;
                }else if(c=='U'){
                    y+=1
                }else if(c=='D'){
                    y-=1
                }
            }

            var row = Math.abs(y) +1;
            var col = Math.abs(x)+1;

            if(m>=row && n>=col){
                console.log(1);
            }else{
                console.log(0);
            }

            process.exit();
        });
    });
});