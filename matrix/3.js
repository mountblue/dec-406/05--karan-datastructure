/*
Given a N X N matrix (M) filled with 1 , 0 , 2 , 3 . 
Your task is to find whether there is a path possible from source to destination,
while traversing through blank cells only. You can traverse up, down, right and left.

A value of cell 1 means Source.

A value of cell 2 means Destination.

A value of cell 3 means Blank cell.

A value of cell 0 means Blank Wall.

Note : there is only single source and single destination.

Author - Karan Kanwal
filename - 1.js
*/

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);


prompts.question("enter the size of the matrix: ",function(n){
    prompts.question("enter your elementns: ", function(nos){
        tempArray = nos.split(' ');
        arr = [];
        var x =0;
        for(var i=0; i<n; i++){
            arr.push([]);
        }
        for(var i=0; i<n; i++){
            for(var j=x; j<x+parseInt(n); j++){
                arr[i].push(tempArray[j]);
            }
            x+=parseInt(n);
        }

        var coords = [];
        for(var i=0; i<arr.length; i++){
            for(var j=0; j<arr.length; j++){
                if(arr[i][j]=='1'){
                    coords.push(i,j);
                    break;
                }
            }
        }
        
        function possible(arr,coords){
            var n = arr.length;
            if(coords[0]-1 >=0){
                if(arr[coords[0]-1][coords[1]] == 2){
                    console.log(1);
                    process.exit();
                }
            }
            if(coords[0]+1< n){
                if(arr[coords[0]+1][coords[1]] == 2){
                    console.log(1);
                    process.exit();
                }
            }
            if(coords[1]-1 >=0){
                if(arr[coords[0]][coords[1]-1] == 2){
                    console.log(1);
                    process.exit();
                }
            }
            if(coords[1]+1< n){
                if(arr[coords[0]][coords[1]+1] == 2){
                    console.log(1);
                    process.exit();
                }
            }


            if(coords[0]-1 >=0){
                if(arr[coords[0]-1][coords[1]] == 3){
                    coords[0] = coords[0]-1;
                    possible(arr,coords);
                }
            }
            if(coords[0]+1< n){
                if(arr[coords[0]+1][coords[1]] == 2){
                    coords[0] = coords[0]+1;
                    possible(arr,coords);
                }
            }
            if(coords[1]-1 >=0){
                if(arr[coords[0]][coords[1]-1] == 2){
                    coords[1] = coords[1]-1;
                    possible(arr,coords);
                }
            }
            if(coords[1]+1< n){
                if(arr[coords[0]][coords[1]+1] == 2){
                    coords[1] = coords[1]+1;
                    possible(arr,coords);
                }
            }
           
            
            console.log(0);
            process.exit();
        }
        
        
        possible(arr,coords);
        process.exit();
    });
});