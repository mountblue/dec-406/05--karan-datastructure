/*
Given an square matrix, turn it by 90 degrees in anti-clockwise direction without using any extra space.

Author - Karan Kanwal
filename - 1.js
*/

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

prompts.question("enter the size of the matrix: ",function(n){
    prompts.question("enter the matrix: ",function(nos){
        var tempArr = nos.split(' ');
        var arr = [];
        for(var i=0; i<n; i++){
            arr.push([]);
        }
        var x=0;
        for(var i=0; i<n; i++){
            for(var j=x; j<x+parseInt(n); j++){
                arr[i].push(tempArr[j]);
            }
            x+=parseInt(n);
        }
        var temp;
        for(var row=0; row<arr.length; row++){
            for(var col=0; col<arr.length/2; col++){
                temp = arr[row][col];
                arr[row][col] = arr[row][n-col-1];
                arr[row][n-col-1] = temp;
            }
        } 
    
        //transpose
        for(var row=0; row<arr.length; row++){
            for(var col=row; col<arr.length; col++){
                temp = arr[row][col];
                arr[row][col] = arr[col][row];
                arr[col][row] = temp;
            }
        }

        console.log(arr);
        process.exit();
    });
});