/*
Given an incomplete Sudoku configuration in terms of a 9x9 2-D square matrix (mat[][])
the task to check if the configuration has a solution or not.

Author - Karan Kanwal
filename - 1.js
*/

var readline = require('readline');
var prompts = readline.createInterface(process.stdin, process.stdout);

function duplicate(arr){
    for(var i=0; i<arr.length-1; i++){
        for(var j=i+1; j<arr.length; j++){
            if(arr[i]!=0 && arr[j]!=0){
                if(arr[i]==arr[j]){
                    return true;
                }
            }
            
        }
    }
    return false;
}

prompts.question('Enter the entries ',function(n){
    var arr = n.split(' ');
    var arr1=[];
    var arr2=[];
    var arr3=[];
    var arr4=[];
    var arr5=[];
    var arr6=[];
    var arr7=[];
    var arr8=[];
    var arr9=[];

    var arrOfarr = [arr1, arr2, arr3, arr4, arr5, arr6, arr7, arr8, arr9];
    var x=0;
    for(var i=0; i<arr.length; i++){
        var temp=0;
        while(temp<9){
            arrOfarr[x].push(arr[i]);
            temp++;
                i++;
        }
        i--;
        x++;
    }
    //check for rows 
    for(var i=0; i<arrOfarr.length; i++){
        if(duplicate(arrOfarr[i])){
            console.log(0);
            process.exit();
        }
    }
    arrOfarr2 = [[],[],[],[],[],[],[],[],[]];
    
   //transpose the matrix
    for(var i=0; i<9; i++){
        for(var j=0; j<9; j++){
            arrOfarr2[j].push(arrOfarr[i][j]);
        }
    }
    
    //check for columns
    for(var i=0; i<arrOfarr.length; i++){
        if(duplicate(arrOfarr2[i])){
            console.log(0);
            process.exit();
        }
    }

    //check for grids
    grid = [[],[],[],[],[],[],[],[],[]];
    for(var i=0; i<9; i++){
        for(var j=0; j<9; j++){

            row = Math.floor(i/3);
            col = Math.floor(j/3);

            index = row*3 + col;
            grid[index].push(arrOfarr[i][j]);
            
        }
    }

    for(var i=0; i<arrOfarr.length; i++){
        if(duplicate(grid[i])){
            console.log(0);
            process.exit();
        }
    }
    console.log(1);
    process.exit();
});